OWLURL=https://haltp.org/files/ol-0.2.c.gz

sure: main
	test/run

main: main.c
	cc -O -o main main.c

ol.c:
	curl $(OWLURL) | gzip -d > ol.c

ol: ol.c
	cc -O -o ol ol.c

main.c: ol main.scm parse.scm tools.scm
	./ol -o main.c main.scm

clean:
	-rm main main.c
	-rm test/*.out

mrproper: clen
	-rm ol ol.c

.phony: sure clean 
