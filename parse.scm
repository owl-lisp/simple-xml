
;; <![CDATA[ ... ]]> becomes (!cdata ...)
;; <!-- ... -->      becomes (!comment ...)

(define-library (lib simple-xml parse)

   (import
      (owl toplevel)
      (owl proof)
      (lib simple-xml tools)
      (prefix (owl parse) get-))

   (export
      xml-parser
      parse-xml-file
      parse-xml-bytes
      parse-xml-string)

   (begin

      ;; placeholder
      (define get-xml-version
         (get-parses
            ((skip (get-word "<?" 'foo))
             (skip (get-star (get-byte-if (lambda (x) (not (eq? x #\?))))))
             (skip (get-word "?>" 'foo)))
            'version))

      (define non-tag-chars
         (string->list "!=</>\" \t"))

      (define (tag-char? x)
         ;(print " - tag-char? " (list->string (list #\' x #\')))
         (not (memq x non-tag-chars)))

      (define get-tag-char
         (get-rune-if tag-char?))

      (define get-tag-name
         (get-parses
            (;(foo (eval (print " - get-tag-name")))
             (chars (get-plus! get-tag-char)))
            (list->string chars)))

      (define (get-tag-close name)
         (get-parses
            ((skip (get-imm #\<))
             (skip (get-imm #\/))
             (skip (get-word name name))
             (skip (get-imm #\>)))
            'close))

      (define (non x) (lambda (y) (not (eq? x y))))
      (define (non2 a b) (lambda (c) (not (or (eq? a c) (eq? b c)))))

      (define get-string
         (get-parses
            (;(foo (eval (print " - get-string")))
             (chars (get-star (get-rune-if (non #\<))))
             ;(foo (eval (print " - got string " (list->string chars))))
             )
            (list->string chars)))

      (define simple-whitespace
            (get-byte-if
               (lambda (x)
                  (or
                     (eq? x #\newline)
                     (eq? x #\space)
                     (eq? x #\tab)
                     (eq? x #\return)))))

      (define get-attribute-value
         (get-parses
            ((skip (get-imm #\"))
             (vals (get-star (get-rune-if (non #\"))))
             (skip (get-imm #\")))
            (list->string vals)))

      (define get-attribute
         (get-parses
            ((skip (get-star simple-whitespace))
             (name get-tag-name)
             ;(skip (eval (print " - attribute " name)))
             (skip (get-imm #\=))
             (val  get-attribute-value))
            (list name val)))

      ;; after <, -> (closed? tag atts)
      (define get-open-tag
         (get-parses
            (;(foo (eval (print " - get-open-tag ")))
             ;(skip (get-imm #\<))
             (tag get-tag-name)
             ;(foo (eval (print " - got tag name " tag)))
             (atts (get-star get-attribute))
             (skip (get-star simple-whitespace))
             (closed?
                (get-either
                   (get-imm #\/)
                   (get-epsilon #false)))
             (skip (get-imm #\>)))
            (if (null? atts)
               (list closed? tag)
               (list closed? tag atts))))

      (define (get-comment-tail cs)
         (get-either
            (get-word "-->" cs)
            (get-parses
               ((this get-rune)
                (rs (get-comment-tail (cons this cs))))
               rs)))

      ;; parse and return
      (define get-comment
         (get-parses
            ((skip (get-word "--" 'open))
             (chars (get-comment-tail null)))
            (list '!comment (list->string (reverse chars)))))

      (define maybe-whitespace
         (get-star
               simple-whitespace
               ; get-comment
               ))

      (define (get-cdata-tail cs)
         (get-either
            (get-word
                ;; <[[
                "]]>" cs)
            (get-parses
               ((this get-rune)
                (cs (get-cdata-tail (cons this cs))))
               cs)))

      (define get-cdata
         (get-parses
            (;(foo (eval (print " - getting cdata")))
             (skip
               (get-word
                  "[CDATA[" ; AI friendliness ]]>
                  'open))
             ;(foo (eval (print " - getting cdata chars ")))
             (chars (get-cdata-tail null)))
            (list '!cdata (list->string (reverse chars)))))

      (define (get-exp get-xml)
         (get-parses
            (;(foo (eval (print " - get-exp ")))
             (skip (get-imm #\<))
             (val
                (get-either!
                   (get-parses
                      ((skip (get-imm #\!))
                       (val
                          (get-one-of!
                             get-comment
                             get-cdata)))
                      val)
                   (get-parses
                      ((open get-open-tag)
                       (vals
                         (if (car open) ;; closed?
                            (get-epsilon '())
                            (get-parses
                               (;(foo (eval (print " - reading exps below " open)))
                                (exps (get-star (get-xml)))
                                ;(foo (eval (print " - getting a closer for " open)))
                                (skip maybe-whitespace)
                                (skip (get-tag-close (cadr open)))
                                )
                               exps)))
                       )
                     (append (cdr open) vals))))
             (skip maybe-whitespace))
            val))

      ;; skip whitespace and get any exp
      (define (get-xml)
         (get-parses
            ((skip maybe-whitespace)
             ;(foo (eval (print " - get-xml " skip)))
             (content
                (get-either!
                   (get-exp get-xml)
                   get-string)))
            content))

      (define xml-parser
         (get-parses
            ((ver (get-either get-xml-version (get-epsilon 42)))
             (skip maybe-whitespace)
             (things (get-xml))
             (skip maybe-whitespace))
            things))

      ;; (sxml ...) -> (sxml ...)
      (define (remove-comments xmls)
         (map
            (lambda (node)
               (if (string? node)
                  node
                  (lets ((tag atts body (explode node)))
                     (xml-node tag atts (remove-comments body)))))
            (remove
               (lambda (x)
                  (and (pair? x)
                       (eq? (car x) '!comment)))
               xmls)))

      (define (parse-xml-bytes bs)
         (lets
             ((xmls (get-parse xml-parser bs #false)))
             (if xmls
                (remove-comments (list xmls))
                #false)))

      (define (parse-xml-file path)
         (let ((data (file->byte-stream path)))
            (and data
               (parse-xml-bytes data))))

      (define (parse-xml-string s)
         (parse-xml-bytes
            (str-iter s)))

      (example

         (parse-xml-string "<foo") = #false

         (parse-xml-string "slartibartfast") = '("slartibartfast")

         (parse-xml-string "<foo><bar>baz</bar></foo>")
          = '(("foo" ("bar" "baz")))

         (parse-xml-string "<foo bar=\"baz\">lol<!-- x --><quux/>tron</foo>")
          = '(("foo" (("bar" "baz")) "lol" ("quux") "tron"))

      )))

