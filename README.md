# Simple XML utilities

## Usage

You can use this library by cloning it to lib/ directory in a
project, and importing it to your REPL or library.

```sh
$ mkdir lib
$ cd lib && git clone https://haltp.org/git/simple-xml.git
$ ol
You see a prompt.
> (import (lib simple-xml parse))
;; Library (lib simple-xml parse) added
```

## Libraries

```
(lib simple-xml parse) ;; parsing functions
(lib simple-xml utils) ;; fold, map, etc
```

## Functions

```scheme
(import (lib simple-xml parse))

(parse-xml-string "<foo") = #false

(parse-xml-string "slartibartfast") = '("slartibartfast")

(parse-xml-string "<foo><bar>baz</bar></foo>")
 = '(("foo" ("bar" "baz")))

(parse-xml-string "<foo bar=\"baz\">lol<!-- x --><quux/>tron</foo>")
 = '(("foo" (("bar" "baz")) "lol" ("quux") "tron"))
```
