

;; sxml = string | (tag [((attribute value) ...)] sxml ...)

(define-library (lib simple-xml tools)

   (import
      (owl toplevel))

   (export
      xml-node          ; tag atts body -> sxml
      xml-fold          ; (state tag atts body -> state) state sxml -> state
      xml-path-fold     ; (state tag atts body path -> state) state sxml path -> state
      xml-map           ; (tag atts body -> sxml) sxml -> sxml    (default bottom up)
      xml-map-top-down
      xml-map-bottom-up
      explode)

   (begin

      (define (xml-node tag atts body)
         (if tag ;; #false in plain strings
            (if (null? atts)
               (cons tag body)
               (cons tag (cons atts body)))
            body))

      (define (attributes? x)
         (or (null? x)
            (and (pair? x)
               (pair? (car x)))))

      ;; xml node -> tag, attribute list, body list
      (define (explode node)
         (cond
            ((null? (cdr node)) (values (car node) null null))
            ((attributes? (cadr node)) (values (car node) (cadr node) (cddr node)))
            (else
               (values (car node) null (cdr node)))))

      ;; fold and keep track of path
      (define (xml-path-fold op state node path)
         (cond
            ((string? node)
               (op state #f null node path)
               ;state
               )
            ((pair? node)
               (lets ((tag atts body (explode node))
                      (state (op state tag atts body path))
                      (path (cons tag path)))
               (fold
                  (λ (state node) (xml-path-fold op state node path))
                  state
                  body)))
            (else
               (print-to stderr "xml-path-fold: odd node: " node)
               state)))

      (define (xml-fold op state node)
         (xml-path-fold
            (lambda (state tag atts body path)
               (op state tag atts body))
            state node '()))

      ;; bottom up
      (define (xml-map-bottom-up op node)
         (cond
            ((string? node) node)
            ((pair? node)
               (cond
                  ((null? (cdr node))
                     (op (car node) null null))
                  ((attributes? (cadr node))
                     (op (car node) (cadr node)
                        (map
                           (lambda (x) (xml-map-bottom-up op x))
                           (cddr node))))
                  (else
                     (op (car node) null
                        (map
                           (lambda (x) (xml-map-bottom-up op x))
                           (cdr node))))))
            ((null? node)
               node)
            (else
               (print-to stderr "xml map: odd node: " node)
               node)))

      (define xml-map xml-map-bottom-up)


      (define (xml-map-top-down op node)
         (cond
            ((string? node) node)
            ((pair? node)
               (lets
                  ((tag atts body (explode node))
                   (node (op tag atts body))
                   (tag atts body (explode node))
                   (body
                      (map
                         (lambda (x) (xml-map-top-down op x))
                          body)))
                  (if (null? atts)
                     (ilist tag body)
                     (ilist tag atts body))))
            ((null? node)
               node)
            (else
               (print-to stderr "xml map 2: odd node: " node)
               node)))

))
