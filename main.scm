

;; load the libarary explicitly
(import
   (lib simple-xml parse)
   (lib simple-xml tools)
   )

(define (indent lst)
   (if (null? lst)
      ""
      (str "   " (indent (cdr lst)))))

(lambda (args)
   (if (= (length args) 2)
      (let ((data (parse-xml-file (cadr args))))
         (write data)
         (print)
         '(begin
            (print "-----8<------")
            (xml-path-fold
               (lambda (state tag atts body path)
                  (if tag
                     (print (indent path) tag ", " atts)
                     (print (indent path) "'" body "'"))
                  state)
               ""
               data
               '())
            (print "-----8<------"))
         )
      (print "usage: main <path.xml>"))
   0)


